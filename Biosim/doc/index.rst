.. Biosim documentation master file, created by
   sphinx-quickstart on Fri Jan 10 13:52:26 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Biosim's documentation!
==================================

This is a package that models the ecosystem of Rossumoya.

The aim was to investigate the long term survivability of
herbivores and carnivores on the island.

Contents:

.. toctree::
   :maxdepth: 2

   animals
   
   landscape
   
   grid
   
   simulation
   
   visualization

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

