# -*-coding: utf-8 -*-
'''
Created on 6. jan. 2014

@author: kaor
'''

from math import exp
import random


class Animal(object):
    '''
    An animal
    '''

    @classmethod
    def change_rand_number(cls, new_random_gen):
        '''
        Changes random number generator for class.

        :param new_random_gen: new random number generator
        '''
        cls.random_number = new_random_gen

    @classmethod
    def get_params(cls, key):
        '''
        Returns the class specific parameter to key.

        :param key: key to parameter in dictionairy 'params'
        :type key: str
        '''
        return cls.params[key]

    @classmethod
    def set_params(cls, param_dict):
        '''
        Changes the default parameters for the class

        :param param_dict: contains the parameters to be changed,
                           key is the parameter name as str
        :type param_dict: dict
        :raises: KeyError, ValueError
        '''

        def is_num(value):
            '''
            Checks if value is a number.
            '''
            try:
                float(value)
                return True
            except ValueError:
                return False

        def interval_check_and_change(key):
            '''
            Checks if chosen parameter is in [0, 1] and sets it if accepted.
            '''
            if (not 0 <= param_dict[key] <= 1 or not is_num(param_dict[key])):
                raise ValueError(key + ' must be a number in [0, 1].')
            cls.params[key] = float(param_dict[key])

        def positive_check_and_change(key):
            '''
            Checks if chosen parameter is positve and sets it if accepted.
            '''
            if not 0 <= param_dict[key] or not is_num(param_dict[key]):
                raise ValueError(key + ' must be a positive number.')
            cls.params[key] = float(param_dict[key])

        for key in param_dict:
            if key not in (cls.params):
                raise KeyError('Invalid parameter name ' + key)

        for key in ('beta',
                    'sigma',
                    'phi_age'):
            if key in param_dict:
                interval_check_and_change(key)

        if 'phi_low' in param_dict and 'phi_high' in param_dict:
            if param_dict['phi_low'] >= param_dict['phi_high']:
                for key in ('phi_low', 'phi_high'):
                    interval_check_and_change(key)
            else:
                raise ValueError('phi_low must be higher-equal phi_high.')
        elif 'phi_low'  in param_dict:
            if param_dict['phi_low'] >= cls.params['phi_high']:
                interval_check_and_change('phi_low')
            else:
                raise ValueError('phi_low must be higher-equal phi_high.')
        elif 'phi_high'  in param_dict:
            if param_dict['phi_high'] <= cls.params['phi_low']:
                interval_check_and_change('phi_high')
            else:
                raise ValueError('phi_low must be higher-equal phi_high.')

        for key in ('w_birth',
                    'w_min',
                    'a_half',
                    'zeta',
                    'mu',
                    'gamma',
                    'omega',
                    'F',
                    'DeltaPhiMax'):
            if key in param_dict:
                positive_check_and_change(key)

        if 'w_half_low' in param_dict and 'w_half_high' in param_dict:
            if param_dict['w_half_low'] <= param_dict['w_half_high']:
                for key in ('w_half_low', 'w_half_high'):
                    positive_check_and_change(key)
            else:
                raise ValueError('w_half_low must be lower-equal w_half_high.')
        elif 'w_half_low'  in param_dict:
            if param_dict['w_half_low'] <= cls.params['w_half_high']:
                positive_check_and_change('w_half_low')
            else:
                raise ValueError('w_half_low must be lower-equal w_half_high.')
        elif 'w_half_high'  in param_dict:
            if param_dict['w_half_high'] >= cls.params['w_half_low']:
                positive_check_and_change('w_half_high')
            else:
                raise ValueError('w_half_low must be lower-equal w_half_high.')

    def __init__(self):
        '''
        An animal needs a species, use subclasses.
        '''
        pass

    def get_weight(self):
        '''
        Returns the weight of the animal.
        '''
        return self.weight

    def ages(self):
        '''
        Increases the age of the animal by one.

        Note: Recalculates fitness and resets migration-check.
        '''
        self.age += 1
        self.fitness_calc()
        self.has_moved = False

    def eats(self, food):
        '''
        Increases the animal's weight.

        Note: Recalculates fitness.

        :param food: amount of food available
        :type food: float
        :returns: amount of food eaten
        :rtype: float
        '''
        if food < self.params['F']:
            self.weight += self.params['beta'] * food
            self.fitness_calc()
            return food
        else:
            self.weight += self.params['beta'] * self.params['F']
            self.fitness_calc()
            return self.params['F']

    def _q(self, x, x_half, phi):
        '''
        A part of the fitness calculation.
        '''
        return 1 / (1 + exp(phi * (x - x_half)))

    def fitness_calc(self):
        '''
        Calculates the fitness of the animal based on age and weight.
        '''

        if self.weight < self.params['w_min']:
            self.fitness = 0
        else:
            self.fitness = (
            self._q(self.age, self.params['a_half'], self.params['phi_age']) *
            self._q(self.weight, self.params['w_half_low'],
                   -self.params['phi_low']) *
            self._q(self.weight, self.params['w_half_high'],
                    self.params['phi_high'])
            )

    def kills(self, herbivore):
        '''
        Checks if the carnivore kills a herbivore.

        :param herbivore: an instance of the herbivore class
        :type herbivore: Herbivore
        :returns: True if carnivore kills prey
        :rtype: bool
        '''
        if self.fitness <= herbivore.fitness:
            return False
        elif self.fitness - herbivore.fitness < self.params['DeltaPhiMax']:
            if self.random_number() < ((self.fitness - herbivore.fitness) /
                                        self.params['DeltaPhiMax']):
                return True
            else:
                return False
        else:
            return True

    def is_full(self, herb_weight):
        '''
        Checks if the carnivore has eaten enough.

        :param herb_weight: the weight of the killed herbivore
        :type herb_weight: float
        :returns: True if the carnivore is full.
        :rtype: bool
        '''
        if herb_weight >= self.params['F']:
            return True
        else:
            return False

    def moves(self):
        '''
        Checks if the animal will move.

        Note: If it does, 'has_moved' is also set to 'True'.

        :returns: True if the animal will move.
        :rtype: bool
        '''
        if (not self.has_moved and
            self.random_number() < self.params['mu'] * self.fitness):
            self.has_moved = True
            return True
        else:
            return False

    def moves_to(self):
        '''
        Checks in which direction the animal wants to migrate.

        :returns: 'north', 'east', south' or 'west' with eqaul probability
        :rtype: str
        '''
        rand_num = self.random_number()
        if rand_num < 0.25:
            return 'north'
        elif rand_num < 0.5:
            return 'east'
        elif rand_num < 0.75:
            return 'south'
        else:
            return 'west'

    def gives_birth(self, N):
        '''
        Checks if the animal gives birth.

        Note: If it does, the animal's weight and fitness are updated.

        :param N: number of animals in the cell
        :type N: int
        :returns: True if the animal gives birth
        :rtype: bool
        '''
        giving_birth = False
        if self.weight >= (self.params['w_min'] +
                           self.params['zeta'] * self.params['w_birth']):
            prob = self.params['gamma'] * self.fitness * (N - 1)
            if prob > 1:
                prob = 1
            if self.random_number() < prob:
                giving_birth = True
                self.weight -= (self.params['zeta'] * self.params['w_birth'])
                self.fitness_calc()
        return giving_birth

    def loses_weight(self):
        '''
        Decreases the weight of the animal.

        Note: Recalculates the animals fitness.
        '''
        self.weight -= self.params['sigma'] * self.weight
        self.fitness_calc()

    def dies(self):
        '''
        Returns True if the animal dies.
        '''
        die = False
        if self.weight < self.params['w_min']:
            die = True
        elif self.random_number() < self.params['omega'] * (1 - self.fitness):
            die = True
        return die


class Herbivore(Animal):
    """
    This is a planteater.

    For more information about the parameters see 'parameters'.
    """
    params = {'w_birth': 8.,
              'beta': 0.4,
              'sigma': 0.05,
              'w_min': 5.,
              'a_half': 40.,
              'phi_age': 0.1,
              'w_half_low': 10.,
              'w_half_high': 60.,
              'phi_low': 0.5,
              'phi_high': 0.2,
              'mu': 0.15,
              'gamma': 0.1,
              'zeta': 2.,
              'omega': 0.01,
              'F': 10.}

    random_number = random.random

    def __init__(self, age=0, weight=params['w_birth']):
        """
        Creates a herbivore.

        :param age: age of the herbivore
        :type age: int
        :param weight: weight of the herbivore
        :type weight: float
        """
        self.age = age
        self.weight = weight
        self.fitness = 0
        self.has_moved = False

        self.fitness_calc()


class Carnivore(Animal):
    """
    This is a predator.

    For more information about the parameters see 'parameters'.
    """
    params = {'w_birth': 6.,
              'beta': 0.75,
              'sigma': 0.1,
              'w_min': 3.,
              'a_half': 40.,
              'phi_age': 0.15,
              'w_half_low': 6.,
              'w_half_high': 40.,
              'phi_low': 0.3,
              'phi_high': 0.1,
              'mu': 0.4,
              'gamma': 0.25,
              'zeta': 2.5,
              'omega': 0.01,
              'F': 15.,
              'DeltaPhiMax': 0.75}

    random_number = random.random

    def __init__(self, age=0, weight=params['w_birth']):
        """
        Creates a carnivore.

        :param age: age of the carnivore
        :type age: int
        :param weight: weight of the carnivore
        :type weight: float
        """
        self.age = age
        self.weight = weight
        self.fitness = 0
        self.has_moved = False

        self.fitness_calc()
