# -*-coding: utf-8 -*-
'''
Created on 16. jan. 2014

@author: jonasni
'''
from animals import Herbivore, Carnivore
import matplotlib.pyplot as plt
import numpy as np


class Visual(object):
    '''
    Performs visualization of the simulation
    '''

    def __init__(self, island, cbarlim_herb, cbarlim_carn):
        '''
        Creates a figure with subplots.

        :param island: multiline string of letters representing
                       different landscapes
        :type island: str
        :param cbarlim_herb: contains min and max of the colorbar (min, max)
                             given as number of herbivores per cell
        :type cbarlim_herb: tuple
        :param cbarlim_carn: contains min and max of the colorbar (min, max)
                             given as number of carnivores per cell
        :type cbarlim_carn: tuple
        '''
        self.fig = plt.figure(figsize=(22, 12))
        self.fig.suptitle(u'Modeling the Ecosystem of Rossumøya', fontsize=20)
        self.fig.text(0.80, 0.035, 'by Jonas Nilsen and Katrin Ortstein',
                      fontsize=12)
        self.colors = {'O': (0.0, 0.0, 1.0),  # blue
                       'J': (0.0, 0.5, 0.0),  # dark green
                       'S': (0.45, 0.9, 0.45),  # light green
                       'D': (0.95, 0.95, 0.45),  # yellow
                       'M': (0.7, 0.7, 0.7)}  # grey
        self.create_map_of_island(island)
        self.create_legend_of_island()
        self.create_pop_density(island, cbarlim_herb, cbarlim_carn)
        self.create_animal_graph()
        self.create_numbers()

    def get_figure(self):
        '''
        Returns figure
        '''
        return self.fig

    def create_map_of_island(self, island):
        '''
        Creates a map of the island.

        :param island: multiline string of letters representing
                       different landscapes
        :type island: str
        '''
        sp1 = self.fig.add_subplot(2, 3, 1)
        map_rgb = [[self.colors[column] for column in row]
                   for row in island.splitlines()]
        sp1.imshow(map_rgb, interpolation='nearest')
        sp1.set_xticks(range(len(map_rgb[0])))
        sp1.set_xticklabels(range(1, 1 + len(map_rgb[0])))
        sp1.set_yticks(range(len(map_rgb)))
        sp1.set_yticklabels(range(1, 1 + len(map_rgb)))
        sp1.set_title(u'Rossumøya')

    def create_legend_of_island(self):
        '''
        Creates a legend to the map of the island.
        '''
        sp1_leg = self.fig.add_subplot(2, 4, 5)
        sp1_leg.axis('off')
        for ix, name in enumerate(('Ocean', 'Mountain', 'Jungle',
                                   'Savannah', 'Desert')):
            sp1_leg.add_patch(plt.Rectangle((0., ix * 0.2), 0.3, 0.1,
                                         edgecolor='k',
                                         facecolor=self.colors[name[0]]))
            sp1_leg.text(0.35, ix * 0.2, name)

    def create_numbers(self):
        '''
        Creates a subplot for text and numbers.
        '''
        numbs = self.fig.add_subplot(2, 4, 6)
        numbs.axis('off')
        self.template1 = '''Current Year: {:5}\n
Total Number of Animals: {:5}\n
Total Number of Herbivores: {:5}\n
Total Number of Carnivores: {:5}'''
        self.txt = numbs.text(0., 0.5, self.template1.format(0, 0, 0, 0),
                                #horizontalalignment='left',
                                verticalalignment='center')
                                #transform=numbs.transAxes)

    def update_numbers(self, curr_year, animal_nums):
        '''
        Updates the numbers.

        :param curr_year: current year of the simulation
        :type curr_year: int
        :param animal_nums: total number of animals on the island per year,
                            organized in lists for every species
        :type animal_nums: dict
        '''
        self.txt.set_text(self.template1.format(curr_year,
                                                animal_nums[Herbivore][-1] +
                                                animal_nums[Carnivore][-1],
                                                animal_nums[Herbivore][-1],
                                                animal_nums[Carnivore][-1]))

    def create_pop_density(self, island, cbarlim_herb, cbarlim_carn):
        '''
        Creates subplots for population densities of herbivores and carnivores.

        :param island: multiline string of letters representing
                       different landscapes
        :type island: str
        :param cbarlim_herb: contains min and max of the colorbar (min, max)
                             given as number of herbivores per cell
        :type cbarlim_herb: tuple
        :param cbarlim_carn: contains min and max of the colorbar (min, max)
                             given as number of carnivores per cell
        :type cbarlim_carn: tuple
        '''
        rows = len(island.splitlines())
        cols = len(island.splitlines()[0])

        self.herbd = self.fig.add_subplot(2, 3, 2)
        self.herbd_im = self.herbd.imshow(np.zeros((rows, cols)),
                                          interpolation='nearest',
                                          vmin=cbarlim_herb[0],
                                          vmax=cbarlim_herb[1])
        self.herbd.set_xticks(range(cols))
        self.herbd.set_xticklabels(range(1, 1 + cols))
        self.herbd.set_yticks(range(rows))
        self.herbd.set_yticklabels(range(1, 1 + rows))
        self.fig.colorbar(self.herbd_im, ax=self.herbd, ticks=[cbarlim_herb[0],
                    (cbarlim_herb[1] - cbarlim_herb[0]) / 2. + cbarlim_herb[0],
                    cbarlim_herb[1]])
        self.herbd.set_title('Density of Herbivores')

        self.carnd = self.fig.add_subplot(2, 3, 3)
        self.carnd_im = self.carnd.imshow(np.zeros((rows, cols)),
                                          interpolation='nearest',
                                          vmin=cbarlim_carn[0],
                                          vmax=cbarlim_carn[1])
        self.carnd.set_xticks(range(cols))
        self.carnd.set_xticklabels(range(1, 1 + cols))
        self.carnd.set_yticks(range(rows))
        self.carnd.set_yticklabels(range(1, 1 + rows))
        self.fig.colorbar(self.carnd_im, ax=self.carnd, ticks=[cbarlim_carn[0],
                    (cbarlim_carn[1] - cbarlim_carn[0]) / 2. + cbarlim_carn[0],
                    cbarlim_carn[1]])
        self.carnd.set_title('Density of Carnivores')

    def update_pop_density(self, animal_cell_nums):
        '''
        Updates subplots for population densities of herbivores and carnivores.

        :param animal_cell_nums: numbers of animals per cell per species,
                                 organized in 2d arrays for every species
        :type animal_cell_nums: dict
        '''
        self.herbd_im.set_data(animal_cell_nums[Herbivore])
        self.carnd_im.set_data(animal_cell_nums[Carnivore])
        # color changes can be made like this cmap=plt.get_cmap('gray_r')

    def create_animal_graph(self):
        '''
        Creates graph for total number of animals on the island.
        '''
        self.animg = self.fig.add_subplot(2, 2, 4)
        self.animg.set_title('Total Number of Animals on the Island')
        self.animg.set_xlabel('Year')
        self.animg.set_ylabel('Number of Animals')

    def update_animal_graph_size(self, years, curr_year):
        '''
        Updates size of the graph for total number of animals on the island.

        :param years: total number of years the simulation is going to run
                      (during this run)
        :type years: int
        :param curr_year: current year of simulation
        :type curr_year: int
        '''
        self.animg.set_xlim(0, curr_year + years)
        self.animg.set_ylim(0, 100)
        if curr_year == 0:
            self.herb_line = self.animg.plot(np.arange(years + 1),
                                             np.nan * np.ones(years + 1),
                                             'g-', label='Herbivores')[0]
            self.carn_line = self.animg.plot(np.arange(years + 1),
                                             np.nan * np.ones(years + 1),
                                             'r-', label='Carnivores')[0]
        else:
            self.herb_line = self.animg.plot(np.arange(curr_year + years + 1),
                                        np.hstack((self.herb_line.get_ydata(),
                                                   np.nan * np.ones(years))),
                                             'g-')[0]
            self.carn_line = self.animg.plot(np.arange(curr_year + years + 1),
                                         np.hstack((self.carn_line.get_ydata(),
                                                    np.nan * np.ones(years))),
                                             'r-')[0]
        plt.legend(loc='best')

    def update_animal_graph(self, curr_year, update_interval, animal_nums):
        '''
        Updates graph for total number of animals on the island.

        :param curr_year: current year of simulation
        :type curr_year: int
        :param update_interval: interval in years between figure updates
        :type update_interval: int
        :param animal_nums: total number of animals on the island per year,
                            organized in lists for every species
        :type animal_nums: dict
        '''
        if curr_year == 0:
            curr_herb_num = animal_nums[Herbivore][curr_year]
            curr_carn_num = animal_nums[Carnivore][curr_year]
            self.update_animal_graph_data(curr_year,
                                          curr_herb_num, curr_carn_num)
        else:
            for rel_curr_year in range(-update_interval, 0):
                curr_herb_num = animal_nums[Herbivore][rel_curr_year]
                curr_carn_num = animal_nums[Carnivore][rel_curr_year]
                self.update_animal_graph_data(curr_year + rel_curr_year + 1,
                                              curr_herb_num, curr_carn_num)

    def update_animal_graph_data(self, year, curr_herb_num, curr_carn_num):
        '''
        Updates data of the graph for total number of animals on the island.

        :param year: year for which the data is going to be updated
        :type year: int
        :param curr_herb_num: total number of herbivores for the current year
        :type curr_herb_num: int
        :param curr_carn_num: total number of carnivores for the current year
        :type curr_carn_num: int
        '''
        if self.animg.get_ylim()[1] < curr_herb_num:
            self.animg.set_ylim(0, curr_herb_num * 1.3)
        elif self.animg.get_ylim()[1] < curr_carn_num:
            self.animg.set_ylim(0, curr_carn_num * 1.3)

        yherbdata = self.herb_line.get_ydata()
        yherbdata[year] = curr_herb_num
        self.herb_line.set_ydata(yherbdata)

        ycarndata = self.carn_line.get_ydata()
        ycarndata[year] = curr_carn_num
        self.carn_line.set_ydata(ycarndata)
