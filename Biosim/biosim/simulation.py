'''
Created on 8. jan. 2014

@author: kaor
'''

from grid import Grid
from animals import Animal, Herbivore, Carnivore
from parameters import DefaultParameters
import landscape as land
import visualization as vis
import random

import matplotlib.pyplot as plt


class Simulation(object):
    '''
    Defines a simulation.
    '''
    def __init__(self, map_of_island='''OOO
OJO
OOO''', random_seed=100, rand_numb_gen=random.random):
        '''
        Creates a simulation with the given settings.

        :param map_of_island: multiline string of letters representing
                              different landscapes
        :type map_of_island: str
        :param random_seed: the random seed to be used
        :type random_seed: int
        :param rand_numb_gen: the new random number generator to be used,
                              default is random.random
        '''
        self.map_of_island = map_of_island
        self.grid = Grid(self.map_of_island)
        self.curr_year = 0
        self.created_pops = 0
        self.number_of_runs = 0
        self.im_counter = 0
        self.total_nums_of_animals = {Herbivore: [], Carnivore: []}
        self.total_nums_of_anim_cells = {Herbivore: [], Carnivore: []}
        self.annual_events = [self.feeding,
                              self.procreation,
                              self.migration,
                              self.aging,
                              self.loss_of_weight,
                              self.death]
        self.species = {'Herbivore': Herbivore, 'Carnivore': Carnivore}

        Animal.change_rand_number(rand_numb_gen)
        random.seed(random_seed)

    def run(self, years=5, starting_population=None, update_interval=1,
            cbarlim_herb=(0, 100), cbarlim_carn=(0, 40),
            save_fig=False, prefix='fig', suffix='png'):
        '''
        Runs the simulation for a number of given years.

        Note: May be used multiple times, but the cbarlim_herb and cbarlim_carn
              parameters will only take effect during the first run of the
              simulation (current year = 0).

        :param years: number of years the simulation will run for
        :type years: int
        :param starting_population: contains dictionaries with info about
                                    location and population on the form:
                                    {'loc': (x, y)}, 'pop': [{animal1_info},
                                    {animal2_info}]}

                                    animal_info has keys:
                                    'weight','age', 'herbivore' or 'carnivore'

        :type starting_population: list
        :param update_interval: interval in years between figure updates
        :type update_interval: int
        :param cbarlim_herb: contains min and max of the colorbar (min, max)
                             given as number of herbivores per cell
        :type cbarlim_herb: tuple
        :param cbarlim_carn: contains min and max of the colorbar (min, max)
                             given as number of carnivores per cell
        :type cbarlim_carn: tuple
        :param save_fig: interval in years between figure saves,
                         saving takes a lot of time!
                         filename:'prefix'_00000.'suffix'
        :type save_fig: int
        :param prefix: name to use while saving figures
        :type prefix: str
        :param suffix: filetype to use while saving figures
                       examples of supported formats are: 'png', 'pdf'
        :type suffix: str
        '''
        plt.ion()
        self.total_years = years
        self.first_year(starting_population, update_interval,
                        cbarlim_herb, cbarlim_carn)

        for _ in range(self.total_years):
            plt.pause(0.0000000001)
            if (self.total_nums_of_animals[Herbivore][-1] +
                self.total_nums_of_animals[Carnivore][-1]) == 0:
                break
            for event in self.annual_events:
                self.loop_through_cells(event)
            self.curr_year += 1

            self.count_animals()
            self.figure_update_save(update_interval, save_fig,
                                    prefix, suffix)

        self.number_of_runs += 1
        raw_input('Enter!')

    def loop_through_cells(self, event):
        '''
        Loops through all the cells and calls the event

        :param event: the event to be called
        :type event: method
        '''
        for i in range(len(self.grid.cell)):
            for j in range(len(self.grid.cell[0])):
                if (not self.grid.cell[i][j].__class__
                    in [land.Ocean, land.Mountain]):
                    event((i, j))

    def count_animals(self):
        '''
        Stores the number of animals on the island.
        '''
        curr_tot_anim, curr_anim_cells = self.grid.count_all_animals()
        for key in curr_tot_anim:
            self.total_nums_of_animals[key].append(curr_tot_anim[key])
            self.total_nums_of_anim_cells[key] = curr_anim_cells[key]

    def feeding(self, (i, j)):
        '''
        Calls the feeding method in the landscape module.

        :param i: the x coordinate to the cell in grid coordinates
        :type i: int
        :param i: the x coordinate to the cell in grid coordinates
        :type i: int
        '''
        self.grid.cell[i][j].feeding()

    def procreation(self, (i, j)):
        '''
        Calls the procreation method in the landscape module.

        :param i: the x coordinate to the cell in grid coordinates
        :type i: int
        :param i: the x coordinate to the cell in grid coordinates
        :type i: int
        '''
        self.grid.cell[i][j].procreation()

    def migration(self, (i, j)):
        '''
        Calls the migration method in the grid module.

        :param i: the x coordinate to the cell in grid coordinates
        :type i: int
        :param i: the x coordinate to the cell in grid coordinates
        :type i: int
        '''
        self.grid.migration((i, j))

    def aging(self, (i, j)):
        '''
        Calls the aging method in the landscape module.

        :param i: the x coordinate to the cell in grid coordinates
        :type i: int
        :param i: the x coordinate to the cell in grid coordinates
        :type i: int
        '''
        self.grid.cell[i][j].aging()

    def loss_of_weight(self, (i, j)):
        '''
        Calls the loss_of_weight method in the landscape module.

        :param i: the x coordinate to the cell in grid coordinates
        :type i: int
        :param i: the x coordinate to the cell in grid coordinates
        :type i: int
        '''
        self.grid.cell[i][j].loss_of_weight()

    def death(self, (i, j)):
        '''
        Calls the death method in the landscape module.

        :param i: the x coordinate to the cell in grid coordinates
        :type i: int
        :param i: the x coordinate to the cell in grid coordinates
        :type i: int
        '''
        self.grid.cell[i][j].death()

    def first_year(self, starting_population, update_interval,
                   cbarlim_herb, cbarlim_carn):
        '''
        Handles startup during the first year of the run.

        Note: Is called the first year of each run, not only during year = 0.

        :param starting_population: contains dictionaries with info about
                                    location and population
        :type starting_population: list
        :param update_interval: interval in years between figure updates
        :type update_interval: int
        :param cbarlim_herb: contains min and max of the colorbar (min, max)
                             given as number of herbivores per cell
        :type cbarlim_herb: tuple
        :param cbarlim_carn: contains min and max of the colorbar (min, max)
                             given as number of carnivores per cell
        :type cbarlim_carn: tuple
        '''
        if self.created_pops < 2 and starting_population == None:
            self.create_pop()
        elif isinstance(starting_population, list):
            self.place_starting_population(starting_population)

        if self.number_of_runs == 0:
            self.count_animals()
            self.plots = vis.Visual(self.map_of_island,
                                    cbarlim_herb, cbarlim_carn)

        self.plots.update_animal_graph_size(self.total_years, self.curr_year)
        self.plots.update_animal_graph(self.curr_year, update_interval,
                                       self.total_nums_of_animals)

    def create_pop(self):
        '''
        Creates a default starting population if none is provided.

        Note: Is never used if starting_population is given!
              Default is 30 herbivores during the first run and 10 carnivores
              during the second run. Age and weight of these animals is
              decided randomly.
        '''
        if self.created_pops == 0:
            for _ in range(30):
                self.grid.add_animal_grid((2, 2),
                Herbivore(random.randrange(0, 80),
                          random.randrange(Herbivore.get_params('w_min'), 80)))
        else:
            for _ in range(10):
                self.grid.add_animal_grid((2, 2),
                Carnivore(random.randrange(0, 60),
                          random.randrange(Carnivore.get_params('w_min'), 60)))
        self.created_pops += 1

    def place_starting_population(self, starting_population):
        '''
        Places the given animals at the given location if it is allowed.

        :param starting_population: contains dictionaries with info about
                                    location and population
        :type starting_population: list
        '''
        for animal_info in starting_population:
            self.check_placement_conditions(animal_info)
            for anim_i in range(len(animal_info['pop'])):
                self.check_animal_conditions(animal_info, anim_i)
                self.grid.add_animal_grid(animal_info['loc'],
                        self.species[animal_info['pop'][anim_i]['species']](
                                     animal_info['pop'][anim_i]['age'],
                                     animal_info['pop'][anim_i]['weight']))
        self.created_pops = 2

    def check_placement_conditions(self, animal_info):
        '''
        Checks if the given location exists inside the grid.

        :param animal_info: contains a dictionary with info about
                            location and population
        :type animal_info: dict
        :raises: IndexError
        '''
        if (not 0 < animal_info['loc'][0] <= self.grid.get_size()[0] or
            not 0 < animal_info['loc'][1] <= self.grid.get_size()[1]):
            raise IndexError('Coordinates are outside the boundaries!')

    def check_animal_conditions(self, animal_info, animal_i):
        '''
        Checks if the info provided about the animal is allowed.

        :param animal_info: contains a dictionary with info about
                            location and population
        :type animal_info: dict
        :param animal_i: the index of the animal
                         in the list in animal_info['pop']
        :type animal_i: int
        :raises: ValueError
        '''
        if animal_info['pop'][animal_i]['age'] < 0:
            raise ValueError('Animal must be born! (negative age confirmed)')
        if (animal_info['pop'][animal_i]['weight'] <
            self.species[animal_info['pop'][animal_i]['species']].
            get_params('w_min')):
            raise ValueError('Weight must be equal-greater than w_min')

    def get_year(self):
        '''
        Returns the number of years that have been simulated.

        :rtype: int
        '''
        return self.curr_year

    def get_number_of_animals(self):
        '''
        Returns the total number of animals on the island.

        :rtype: int
        '''
        return (self.total_nums_of_animals[Herbivore][-1] +
                self.total_nums_of_animals[Carnivore][-1])

    def get_number_of_animals_by_species(self):
        '''
        Returns the total number of animals on the island per species.

        :returns: amount of animals as a number per species,
                  keys are 'herbivore', 'carnivore'
        :rtype: dict
        '''
        return {'herbivores': self.total_nums_of_animals[Herbivore][-1],
                'carnivores': self.total_nums_of_animals[Carnivore][-1]}

    def get_per_cell_animal_count(self):
        '''
        Returns the numbers of animals per cell per species.

        :returns: a dictionary containing grid sized arrays for each species,
                  keys are 'herbivore', 'carnivore'
        :rtype: dict
        '''
        return {'herbivores': self.total_nums_of_anim_cells[Herbivore],
                'carnivores': self.total_nums_of_anim_cells[Carnivore]}

    def set_parameters(self, herb_par=DefaultParameters.herb_params,
                       carn_par=DefaultParameters.carn_params,
                       jungle_par=DefaultParameters.jungle_params,
                       savannah_par=DefaultParameters.savannah_params):
        '''
        Changes parameters to those given.

        Note: If no dictionary is given for a class it will be changed
              back to default.

        :param herb_par: the herbivore parameters to be changed
        :type herb_par: dict
        :param carn_par: the carnivore parameters to be changed
        :type herb_par: dict
        :param jungle_par: the jungle parameters to be changed
        :type jungle_par: dict
        :param savannah_par: the savannah parameters to be changed
        :type savannah_par: dict
        '''

        parameters = {Herbivore: herb_par,
                      Carnivore: carn_par,
                      land.Jungle: jungle_par,
                      land.Savannah: savannah_par}

        for key in parameters:
            key.set_params(parameters[key])

    def figure_update_save(self, update_interval, save_fig,
                           prefix, suffix):
        '''
        Handles updating and saving of the figure.

        :param update_interval: interval in years between figure updates
        :type update_interval: int
        :param save_fig: interval in years between figure saves,
                         saving takes a lot of time!
                         filename: 'prefix'_00000.'suffix'
        :type save_fig: int
        :param prefix: name to use while saving figures
        :type prefix: str
        :param suffix: filetype to use while saving figures
                       examples of supported formats are: 'png', 'pdf'
        :type suffix: str
        '''
        if self.curr_year % update_interval == 0:
                self.plots.update_numbers(self.curr_year,
                                          self.total_nums_of_animals)
                self.plots.update_pop_density(self.total_nums_of_anim_cells)
                self.plots.update_animal_graph(self.curr_year,
                                               update_interval,
                                               self.total_nums_of_animals)
        if save_fig:
            if self.curr_year % save_fig == 0:
                self.save_figure(prefix, suffix)

    def save_figure(self, prefix, suffix):
        '''
        Saves the figure to file.

        :param prefix: name to use while saving figures
        :type prefix: str
        :param suffix: filetype to use while saving figures
                       examples of supported formats are: 'png', 'pdf'
        :type suffix: str
        '''
        plt.savefig('{base}_{num:05d}.{type}'.format(base=prefix,
                                                     num=self.im_counter,
                                                     type=suffix))
        self.im_counter += 1
