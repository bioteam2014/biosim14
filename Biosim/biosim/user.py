'''
Created on 20. jan. 2014

@author: jonasni
'''

from simulation import Simulation

with open('rossumoya.txt') as f:
    island = f.read().rstrip()

starting_animals = [{'loc': (2, 2),
                    'pop': []}]
herb = {'species': 'Herbivore',
        'age': 3,
        'weight': 20}
carn = {'species': 'Carnivore',
        'age': 4,
        'weight': 25}
for _ in range(30):
    starting_animals[0]['pop'] += [herb]
for _ in range(10):
    starting_animals[0]['pop'] += [carn]

random_seed = 123456

sim = Simulation(island, random_seed)

sim.run(100)
sim.run(100)

print sim.get_year()
print sim.get_number_of_animals()
print sim.get_number_of_animals_by_species()
print sim.get_per_cell_animal_count()

sim.run(1000)
