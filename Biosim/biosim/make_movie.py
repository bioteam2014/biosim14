'''
Created on 22. jan. 2014

@author: kaor
'''

import subprocess

# update this variable to point to your ffmpeg binary
_FFMPEG_BINARY = r'I:\tools\ffmpeg-git-win64-static\bin\ffmpeg'


def make_movie(img_base):
        """
        Creates  movie from visualization images saved.

        .. :note:
            Requires ffmpeg

        The movie is stored as img_base + '.avi'
        """

        if img_base is None:
            raise RuntimeError("No filename defined.")

        try:
            subprocess.check_call([_FFMPEG_BINARY, '-r', '5', '-y', '-i',
                                   '{}_%05d.png'.format(img_base),
                                   '{}.avi'.format(img_base)])
        except subprocess.CalledProcessError as err:
            print "ERROR: ffmpeg failed:", err


if __name__ == '__main__':

    make_movie('graphics/fig')
