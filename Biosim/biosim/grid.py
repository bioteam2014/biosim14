'''
Created on 7. jan. 2014

@author: jonasni
'''
import landscape as land
from animals import Herbivore, Carnivore
import numpy as np


class Grid(object):
    '''
    Collection of landscapes in a nested list forming the island.
    '''
    def __init__(self, grid_cells):
        '''
        Creates a grid with given cells.

        :param grid_cells: multiline string of letters representing
                           different landscapes
        :type grid_cell: str
        '''
        self.cell = []
        self.cell_types = {'O': land.Ocean,
                           'J': land.Jungle,
                           'S': land.Savannah,
                           'D': land.Desert,
                           'M': land.Mountain}

        self.create_island(grid_cells)
        self.num_of_rows = len(self.cell)
        self.num_of_cols = len(self.cell[0])

    def create_island(self, grid_cells):
        '''
        Creates a rectangular island with given cells

        :param grid_cells: multiline string of letters representing
                           different landscapes
        :type grid_cell: str
        :raises: ValueError
        '''
        self.split_grid = grid_cells.splitlines()

        for row in range(len(self.split_grid)):
            row_of_cells = []
            if len(self.split_grid[row]) != len(self.split_grid[0]):
                raise ValueError('All rows must be of equal length!')

            for col in range(len(self.split_grid[0])):
                cell_type = self.split_grid[row][col]
                self.island_test(row, col)
                if cell_type in self.cell_types:
                    row_of_cells.append(self.cell_types[cell_type]
                                        (row + 1, col + 1))
                else:
                    raise ValueError(self.split_grid[row][col] +
                                     ' is not a known celltype')
            self.cell.append(row_of_cells)

    def island_test(self, row, col):
        '''
        Checks if the island is surrounded by ocean.

        :param row: index of row
        :type row: int
        :param col: index of col
        :type col: int
        :raises: ValueError
        '''
        if row == 0 or row == len(self.split_grid) - 1:
            if self.split_grid[row][col] != 'O':
                raise ValueError('Island must be surrounded by ocean!')
        elif col == 0 or col == len(self.split_grid[row]) - 1:
            if self.split_grid[row][col] != 'O':
                raise ValueError('Island must be surrounded by ocean!')

    def add_animal_grid(self, landscp_pos, animal):
        '''
        Adds an specific animal to a cell if possible.

        :param landscp_pos: position of cell in landscape coordinates,
                            (0, 0) in grid = (1, 1) in landscape
        :type landscp_pos: tuple
        :param animal: the specific animal
        :type species: `Animal`
        :raises: TypeError
        '''
        if (not self.cell[landscp_pos[0] - 1][landscp_pos[1] - 1].
            add_animal(animal)):
            raise TypeError('This landscape is not walkable!')

    def count_all_animals(self):
        '''
        Counts all the animals on the island.

        :returns: Total number of animals per species on the island (dict) and
                  the numbers of animals per cell per species (dict)
        :rtype: tuple
        '''
        number_of_animals = {Herbivore: 0, Carnivore: 0}
        number_of_animals_per_cell = {Herbivore: [], Carnivore: []}
        for i in range(self.num_of_rows):
            for j in range(self.num_of_cols):
                temp = self.cell[i][j].get_number_of_animals()
                for key in temp:
                    number_of_animals[key] += temp[key]
                    number_of_animals_per_cell[key] += [temp[key]]
        number_of_animals_per_cell[Herbivore] = (np.array(
                                number_of_animals_per_cell[Herbivore]).
                                reshape(self.num_of_rows, self.num_of_cols))
        number_of_animals_per_cell[Carnivore] = (np.array(
                                number_of_animals_per_cell[Carnivore]).
                                reshape(self.num_of_rows, self.num_of_cols))
        return number_of_animals, number_of_animals_per_cell

    def migration(self, loc):
        '''
        Adds migrating animals to new cell and removes them from the old one.

        :param loc: coordinates of the old cell in grid coordinates
        :type loc: tuple
        '''
        tries_to_move = self.cell[loc[0]][loc[1]].get_migrating_animals()
        animals_that_go = []
        for animal_info in tries_to_move:
            neighbour = self.get_cell(animal_info[1])
            if neighbour.add_animal(animal_info[0]):
                animals_that_go.append(animal_info[0])
        self.cell[loc[0]][loc[1]].remove_migrating_animals(animals_that_go)

    def get_cell(self, landscp_pos):
        '''
        Returns a cell in grid coordinates from landscape coordinates.

        :param landdscp_pos: position of the cell in landscape coordinates,
                             (0, 0) in grid = (1, 1) in landscape
        :type landdscp_pos: tuple
        '''
        return self.cell[landscp_pos[0] - 1][landscp_pos[1] - 1]

    def get_size(self):
        '''
        Returns the size of the grid as a tuple (rows, cols).
        '''
        return self.num_of_rows, self.num_of_cols
