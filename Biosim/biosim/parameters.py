'''
Created on 6. jan. 2014

@author: kaor
'''


class DefaultParameters(object):
    '''
    Default parameters for Herbivore, Carnivore, Jungle and Savannah.
    '''
    herb_params = {'w_birth': 8.,   # the animal's weight at birth
                   'beta': 0.4,     # factor for weight increase after eating
                   'sigma': 0.05,   # factor for annual weight decres
                   'w_min': 5.,     # minimum weight an animal can have
                   'a_half': 40.,   # used for the fitness calculation
                   'phi_age': 0.1,  # used for the fitness calculation
                   'w_half_low': 10.,  # used for the fitness calculation
                   'w_half_high': 60.,  # used for the fitness calculation
                   'phi_low': 0.5,  # used for the fitness calculation
                   'phi_high': 0.2,  # used for the fitness calculation
                   'mu': 0.15,      # the animal's mobility
                   'gamma': 0.1,    # used for prpbability of procreation
                   'zeta': 2.,      # weight loss factor after giving birth
                   'omega': 0.01,   # used for probability of death
                   'F': 10.}        # maximum food an animal eats per year

    carn_params = {'w_birth': 6.,
                   'beta': 0.75,
                   'sigma': 0.1,
                   'w_min': 3.,
                   'a_half': 40.,
                   'phi_age': 0.15,
                   'w_half_low': 6.,
                   'w_half_high': 40.,
                   'phi_low': 0.3,
                   'phi_high': 0.1,
                   'mu': 0.4,
                   'gamma': 0.25,
                   'zeta': 2.5,
                   'omega': 0.01,
                   'F': 15.,
                   'DeltaPhiMax': 0.75}  # used for probability of killing prey

    jungle_params = {'fmax': 500.,   # maximum food available in landscape
                     'alpha': 1.}   # factor for yearly growth of food

    savannah_params = {'fmax': 300.,
                       'alpha': 0.3}
