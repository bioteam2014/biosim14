'''
Created on 7. jan. 2014

@author: jonasni
'''
from animals import Herbivore, Carnivore


class Landscape(dict):
    '''
    A landscape
    '''

    relative_dir = {'north': (-1, 0),
                    'east': (0, 1),
                    'south': (1, 0),
                    'west': (0, -1)}

    @classmethod
    def set_params(cls, param_dict):
        '''
        Changes the default parameters for the class

        :param param_dict: contains the parameters to be changed,
                           key is the parameter name as str
        :type param_dict: dict
        :raises: KeyError, ValueError
        '''
        def is_num(value):
            '''
            Checks if input value is a number.
            '''
            try:
                float(value)
                return True
            except ValueError:
                return False

        def interval_check_and_change(key):
            '''
            Checks if chosen parameter is in [0, 1] and sets it if accepted.
            '''
            if (not 0 <= param_dict[key] <= 1 or not is_num(param_dict[key])):
                raise ValueError(key + ' must be a number in [0, 1].')
            cls.params[key] = float(param_dict[key])

        def positive_check_and_change(key):
            '''
            Checks if chosen parameter is positve and sets it if accepted.
            '''
            if not 0 <= param_dict[key] or not is_num(param_dict[key]):
                raise ValueError(key + ' must be a positive number.')
            cls.params[key] = float(param_dict[key])

        for key in param_dict:
            if key not in (cls.params):
                raise KeyError('Invalid parameter name ' + key)

        if 'fmax' in param_dict:
            positive_check_and_change('fmax')

        if 'alpha' in param_dict:
            interval_check_and_change('alpha')

    def __init__(self):
        '''
        A landscape needs to be specified, use subclasses.
        '''
        self.update({Herbivore: [], Carnivore: []})
        self.number_of_animals = {Herbivore: 0, Carnivore: 0}

    def food_decrease(self, decrease):
        '''
        Decreases the amount of edible plants in the current landscape.

        :param decrease: the amount of food to reduce
        :type decrease: float
        '''
        self.currfood -= decrease
        if self.currfood < 0:
            raise ValueError('Current food in area can not be negative!')

    def add_animal(self, animal):
        '''
        Adds a specific animal to the current landscape.

        :param animal: the specific animal
        :type species: `Animal`
        '''
        if self.is_walkable:
            species = animal.__class__
            self[species].append(animal)
            return True
        else:
            return False

    def remove_animal(self, animal):
        '''
        Removes a specific animal from the current landscape.

        :param animal: the specific animal
        :type species: `Animal`
        '''
        species = animal.__class__
        self[species].remove(animal)

    def sort_animals(self, species):
        '''
        Sorts the animals in the current landscape according to fitness.

        Note: Lowest-->Highest

        :param species: type of animal
        :type species: class
        '''
        self[species].sort(cmp=lambda a1, a2: cmp(a1.fitness, a2.fitness))

    def count_animals_in_cell(self):
        '''
        Stores the number of animals in the current landscape.
        '''
        for species in self:
            self.number_of_animals[species] = len(self[species])

    def get_number_of_animals(self):
        '''
        Returns the number of animals in the current landscape.

        :returns: number of animals,
                  key is the class of the animal
        :rtype: dict
        '''
        self.count_animals_in_cell()
        return self.number_of_animals

    def feeding(self):
        '''
        Handles feeding and yearly growth of plants.
        '''
        self.yearly_growth()
        self.feeding_herbs()
        self.feeding_carns()

    def feeding_herbs(self):
        '''
        The local herbivores eat.

        Note: Herbivores eat in accordance to fitness (highest-->lowest).
              Reduces food in cuurent landscape.
        '''
        self.sort_animals(Herbivore)
        if self.currfood != 0:
            for animal in self[Herbivore][::-1]:
                if self.currfood == 0:
                    break
                else:
                    self.food_decrease(animal.eats(self.currfood))

    def feeding_carns(self):
        '''
        The local carnivores try to eat.

        Note: Carnivores eat in accordance to fitness (highest-->lowest).
              Removes herbivores that die during carnivore feeding.
        '''
        self.sort_animals(Carnivore)
        for carn in self[Carnivore][::-1]:
            eaten_thus_far = 0
            surviving_herbs = []
            for i in range(len(self[Herbivore])):
                if carn.kills(self[Herbivore][i]):
                    eaten_thus_far += (carn.eats(self[Herbivore][i].
                                                 get_weight()))
                    if carn.is_full(eaten_thus_far):
                        surviving_herbs += self[Herbivore][i + 1:]
                        break
                else:
                    surviving_herbs.append(self[Herbivore][i])
            self[Herbivore] = surviving_herbs

    def procreation(self):
        '''
        Creates a newborn for each animal that gives birth.
        '''
        self.count_animals_in_cell()
        for species in self:
            for anim_pos in range(self.number_of_animals[species]):
                if (self[species][anim_pos].
                    gives_birth(self.number_of_animals[species])):
                    self[species].append(species())

    def get_migrating_animals(self):
        '''
        Returns animals that tries to move.

        :returns: animals and relative coordinates to destination,
                  nested list with elements like [animal, (rel_x, rel_y)]
        :rtype: list
        '''
        able_to_move = [[animal, animal.moves_to()]
                        for animal in self[Herbivore] + self[Carnivore]
                        if animal.moves()]
        tries_to_move = [[animal_info[0],
                          (self.pos[0] + self.relative_dir[animal_info[1]][0],
                           self.pos[1] + self.relative_dir[animal_info[1]][1])]
                         for animal_info in able_to_move]
        return tries_to_move

    def remove_migrating_animals(self, animals_that_migrate):
        '''
        Removes animals that migrate from the current landscape.

        :param animals_that_migrate: the animals that migrate
        :type animals_that_migrate: list
        '''
        not_migrating = [animal for animal
                        in self[Herbivore] + self[Carnivore]
                        if animal not in animals_that_migrate]
        herbs = []
        carns = []
        for animal in not_migrating:
            if animal.__class__ == Carnivore:
                carns.append(animal)
            elif animal.__class__ == Herbivore:
                herbs.append(animal)
        self.update({Herbivore: herbs, Carnivore: carns})

    def aging(self):
        '''
        Increases the age of all animals in the current landscape.
        '''
        for animal in self[Herbivore] + self[Carnivore]:
            animal.ages()

    def loss_of_weight(self):
        '''
        Reduces the weight of all animals in the current landscape.
        '''
        for animal in self[Herbivore] + self[Carnivore]:
            animal.loses_weight()

    def death(self):
        '''
        Removes animals that die in the current landscape.

        Note: Does not handle death during hunting.
        '''
        for species in self:
            self[species] = [animal for animal in self[species]
                             if not animal.dies()]


class Mountain(Landscape):
    '''
    A landscape type that no animals may walk on.
    '''

    def __init__(self, x=None, y=None):
        '''
        Creates one mountain.

        :param x: the x coordinate of the mountain
        :type x: int
        :param y: the x coordinate of the mountain
        :type y: int
        '''
        Landscape.__init__(self)
        self.pos = (x, y)
        self.is_walkable = False


class Ocean(Landscape):
    '''
    A landscape type that no animals may walk on.
    '''

    def __init__(self, x=None, y=None):
        '''
        Creates one ocean.

        :param x: the x coordinate of the ocean
        :type x: int
        :param y: the x coordinate of the ocean
        :type y: int
        '''
        Landscape.__init__(self)
        self.pos = (x, y)
        self.is_walkable = False


class Desert(Landscape):
    '''
    A landscape type with no food for planteaters.
    '''

    def __init__(self, x=None, y=None):
        '''
        Creates one desert.

        :param x: the x coordinate of the desert
        :type x: int
        :param y: the x coordinate of the desert
        :type y: int
        '''
        Landscape.__init__(self)
        self.pos = (x, y)
        self.is_walkable = True

        self.currfood = 0.

    def yearly_growth(self):
        '''
        Growth of food (plants) in the desert each year.
        '''
        self.currfood += 0


class Jungle(Landscape):
    '''
    A landscape type with a lot of food for planteaters.
    '''

    params = {'fmax': 500.,
              'alpha': 1.}

    def __init__(self, x=None, y=None):
        '''
        Creates one jungle.

        :param x: the x coordinate of the jungle
        :type x: int
        :param y: the x coordinate of the jungle
        :type y: int
        '''
        Landscape.__init__(self)
        self.pos = (x, y)
        self.is_walkable = True
        self.fmax = self.params['fmax']
        self.alpha = 1.

        self.currfood = self.params['fmax']

    def yearly_growth(self):
        '''
        Growth of food (plants) in the jungle each year.
        '''
        self.currfood += (self.params['alpha'] *
                          (self.params['fmax'] - self.currfood))


class Savannah(Landscape):
    '''
    A landscape type with food for planteaters.
    '''

    params = {'fmax': 300.,
              'alpha': 0.3}

    def __init__(self, x=None, y=None):
        '''
        Creates one savannah.

        :param x: the x coordinate of the savannah
        :type x: int
        :param y: the x coordinate of the savannah
        :type y: int
        '''
        Landscape.__init__(self)
        self.pos = (x, y)
        self.is_walkable = True

        self.currfood = self.params['fmax']

    def yearly_growth(self):
        '''
        Growth of food (plants) in the savannah each year.
        '''
        self.currfood += (self.params['alpha'] *
                          (self.params['fmax'] - self.currfood))
