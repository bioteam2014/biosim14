======
Biosim
======

This Project simulates the animal population on rossumoya

Contents
--------

- biosim: Python package for simulation of animal population on rossumoya
- tests : Tests for biosim package