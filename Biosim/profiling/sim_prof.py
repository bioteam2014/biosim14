'''
Run profiling with cProfile and create graph.

Original author Hans E. Plesser
'''

import cProfile
import pstats
import subprocess

from tools.new_gprof2dot import run_gprof2dot

from biosim.simulation import Simulation

# Location of helper programs on HEP's laptop
# _DOT = '/opt/local/bin/dot'

# Location of helper programs on TF02 machines
_DOT = r'I:\tools\Graphviz\bin\dot'

if __name__ == '__main__':

    with open('rossumoya.txt') as f:
        island = f.read().rstrip()

    starting_animals = [{'loc': (2, 2),
                         'pop': []}]
    herb = {'species': 'Herbivore',
            'age': 3,
            'weight': 20}
    carn = {'species': 'Carnivore',
            'age': 4,
            'weight': 25}
    for _ in range(30):
        starting_animals[0]['pop'] += [herb]
    for _ in range(10):
        starting_animals[0]['pop'] += [carn]

    profFile = 'biosim.prof'
    cProfile.run('Simulation(island, 123456).run(200, starting_animals)',
                 profFile)
    prof = pstats.Stats(profFile)
    prof.strip_dirs()
    prof.sort_stats('cumulative').print_stats()

    try:
        run_gprof2dot(r'-f pstats -o {file}.dot {file}'.format(file=profFile))
        subprocess.check_call([_DOT, '-Tpdf', '-o',
                               r'{}.pdf'.format(profFile),
                               r'{}.dot'.format(profFile)])
    except subprocess.CalledProcessError as err:
        print "Conversion failed with error:", err

    print "Profile graph stored as {file}.pdf".format(file=profFile)
