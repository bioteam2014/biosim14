'''
Created on 7. jan. 2014

@author: kaor
'''
import unittest

from biosim.grid import Grid
from biosim.landscape import Jungle
from biosim.animals import Herbivore, Carnivore
import tools.plmock as plmock  # This module was written by Hans E. Plesser
import random


class Test(unittest.TestCase):
    '''
    atm for a single cell
    '''
    def tearDown(self):
        Herbivore.set_params({'mu': 0.15})
        Carnivore.set_params({'mu': 0.4})
        Herbivore.change_rand_number(random.random)
        Carnivore.change_rand_number(random.random)

    def testCreateGrid(self):
        Grid('''OOO
OJO
OOO''')

    def testNumberOfRows(self):
        grid = Grid('''OOOOO
OJJSO
ODSMO
OOOOO''')
        #self.assertEqual(22, len(G.gridlist))
        self.assertEqual(4, grid.num_of_rows)

    def testNumberOfColumns(self):
        grid = Grid('''OOOOO
OJJSO
ODSMO
OOOOO''')
        #self.assertEqual(21, len(G.gridlist[0]))
        self.assertEqual(5, grid.num_of_cols)

    def testLandscapeInCell(self):
        grid = Grid('''OOOOO
OSJDO
OOOOO''')
        self.assertIsInstance(grid.cell[1][2], Jungle)

    def testAddingAnimal(self):
        ''' Use landscape coordinates!!! '''
        grid = Grid('''OOO
OJO
OOO''')
        herb = Herbivore()
        grid.cell[2][2].update({Herbivore: [herb]})
        grid.add_animal_grid((2, 2), herb)
        self.assertIn(herb, grid.cell[2][2][Herbivore])

    def testCountingAllAnimals(self):
        grid = Grid('''OOO
OJO
OOO''')
        herb1, herb2 = Herbivore(), Herbivore()
        carn = Carnivore()
        grid.cell[1][1].update({Herbivore: [herb1, herb2],
                                Carnivore: [carn]})
        self.assertEqual(3, grid.count_all_animals()[0][Herbivore] +
                         grid.count_all_animals()[0][Carnivore])

    def testWrongInput(self):
        self.assertRaises(ValueError, Grid, '''OOOOO
OOJSO
ODMODO
OOOOO''')
        self.assertRaises(ValueError, Grid, '''OOOOO
OJSDO
ODMO
OOOOO''')
        self.assertRaises(ValueError, Grid, '''OJIS
DMOD''')

    def testMigration(self):
        grid = Grid('''OOOOO
OJJJO
OOOOO''')
        herb1, herb2 = Herbivore(), Herbivore()
        carn = Carnivore()
        grid.cell[1][1].update({Herbivore: [herb1, herb2],
                                Carnivore: [carn]})
        Herbivore.set_params({'mu': 20})
        Carnivore.set_params({'mu': 20})
        rand_nums_herb = [1., 0.30, 1., 0.27]
        rand_nums_carn = [1., 0.49]
        Herbivore.change_rand_number(
                            plmock.FuncReturningValueSequence(rand_nums_herb))
        Carnivore.change_rand_number(
                            plmock.FuncReturningValueSequence(rand_nums_carn))
        grid.migration((1, 1))
        self.assertEqual(0, len(grid.cell[1][1][Herbivore] +
                                grid.cell[1][1][Carnivore]))
        self.assertEqual(3, len(grid.cell[1][2][Herbivore] +
                                grid.cell[1][2][Carnivore]))
        self.assertEqual(0, len(grid.cell[1][3][Herbivore] +
                                grid.cell[1][3][Carnivore]))

    def testIsland(self):
        self.assertRaises(ValueError, Grid, 'J')

    def testGetSize(self):
        grid = Grid('''OOOOO
OJJJO
OOOOO''')
        self.assertEqual((3, 5), grid.get_size())


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
