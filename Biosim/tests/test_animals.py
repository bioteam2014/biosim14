'''
Created on 6. jan. 2014

@author: jonasni
'''
import unittest

from biosim.animals import Herbivore, Carnivore
import random


class Test(unittest.TestCase):

    def testCreate(self):
        Herbivore()
        Carnivore()

    def testNewbornAgeZero(self):
        newborn = Herbivore()
        self.assertEqual(0, newborn.age)

    def testNewbornWeight(self):
        newborn = Herbivore()
        self.assertEqual(newborn.params['w_birth'], newborn.weight)

    def testAges(self):
        herb = Herbivore(4, 40)
        herb.ages()
        self.assertEqual(5, herb.age)
        self.assertFalse(herb.has_moved)

    def testAgesFitness(self):
        herb = Herbivore(4, 40)
        herb.ages()
        self.assertAlmostEqual(0.9532285, herb.fitness)

    def testEnoughFodder(self):
        herb = Herbivore(4, 40)
        herb.eats(12)
        self.assertEqual(44, herb.weight)

    def testNotEnoughFodder(self):
        herb = Herbivore(4, 40)
        herb.eats(5)
        self.assertEqual(42, herb.weight)

    def testEnoughFodderFitness(self):
        herb = Herbivore(4, 40)
        herb.eats(12)
        self.assertAlmostEqual(0.9352789, herb.fitness)

    def testNotEnoughFodderFitness(self):
        herb = Herbivore(4, 40)
        herb.eats(5)
        self.assertAlmostEqual(0.9475133, herb.fitness)

    def testFoodLeftEnoughFodder(self):
        herb = Herbivore(4, 40)
        self.assertEqual(10, herb.eats(12))

    def testFoodLeftNotEnoughFodder(self):
        herb = Herbivore(4, 40)
        self.assertEqual(5, herb.eats(5))

    def testMoves(self):
        herb = Herbivore(4, 40)
        Herbivore.change_rand_number(lambda x: 0.1)
        self.assertTrue(herb.moves())
        self.assertTrue(herb.has_moved)
        Herbivore.change_rand_number(random.random)

    def testMovesTo(self):
        herb = Herbivore(4, 40)
        Herbivore.change_rand_number(lambda x: 0.1)
        self.assertEqual('north', herb.moves_to())
        Herbivore.change_rand_number(random.random)

    def testNewbornFitness(self):
        newborn = Herbivore()
        self.assertAlmostEqual(0.2640961, newborn.fitness)

    def testGivingBirthFalseNumber(self):
        herb = Herbivore(4, 40)
        self.assertTrue(not herb.gives_birth(1))

    def testGivingBirthFalseWeight(self):
        herb = Herbivore(4, 16)
        self.assertTrue(not herb.gives_birth(2))

    def testGivingBirthTrue(self):
        herb = Herbivore(4, 40)
        self.assertTrue(herb.gives_birth(1000))

    def testWeightLoss(self):
        herb = Herbivore(4, 30)
        herb.loses_weight()
        self.assertEqual(28.5, herb.weight)

    def testDeath(self):
        herb = Herbivore()
        herb.dies()

    def testDeathFalse(self):
        herb = Herbivore(4, 40)
        self.assertTrue(not herb.dies())

    def testDeathTrue(self):
        herb = Herbivore(4, 4)
        self.assertTrue(herb.dies())

    def testCarnKillsHerb(self):
        herb = Herbivore(4, 4)
        carn = Carnivore(4, 20)
        self.assertTrue(carn.kills(herb))

    def testCarnDoesNotKillHerb(self):
        herb = Herbivore(4, 40)
        carn = Carnivore(4, 100)
        self.assertFalse(carn.kills(herb))

    def testCarnKillsHerbPredefined(self):
        herb = Herbivore(40, 40)
        carn = Carnivore(4, 20)
        Carnivore.change_rand_number(lambda x: 0.49)
        self.assertTrue(carn.kills(herb))
        Carnivore.change_rand_number(lambda x: 0.5)
        self.assertFalse(carn.kills(herb))
        Carnivore.change_rand_number(random.random)

    def testGetWeight(self):
        herb = Herbivore(4, 30)
        self.assertEqual(30, herb.get_weight())

    def testIsFull(self):
        carn = Carnivore(4, 20)
        self.assertTrue(carn.is_full(carn.params['F']))
        self.assertFalse(carn.is_full(carn.params['F'] - 1.))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
