'''
Created on 7. jan. 2014

@author: jonasni
'''
import unittest

from biosim.animals import Herbivore, Carnivore
import biosim.landscape as land
import random
import tools.plmock as plmock  # This module was written by Hans E. Plesser


class Test(unittest.TestCase):

    def tearDown(self):
        jcell = land.Jungle()
        Herbivore.change_rand_number(random.random)
        Carnivore.change_rand_number(random.random)
        Herbivore.set_params({'mu': 0.15})
        Herbivore.set_params({'gamma': 0.1})
        jcell.set_params({'fmax': 500, 'alpha': 1.})

    def testSetParams(self):
        jcell = land.Jungle()
        jcell.set_params({'fmax': 1000, 'alpha': 0.5})

        self.assertEqual(1000, jcell.params['fmax'])
        self.assertEqual(0.5, jcell.params['alpha'])

        self.assertRaises(KeyError, land.Jungle.set_params,
                          {'trallala': 1})
        self.assertRaises(ValueError, land.Jungle.set_params,
                          {'fmax': -0.001})
        self.assertRaises(ValueError, land.Jungle.set_params,
                          {'alpha': -0.001})
        self.assertRaises(ValueError, land.Jungle.set_params,
                          {'alpha': 1.001})
        self.assertRaises(ValueError, land.Jungle.set_params,
                          {'fmax': 'string'})
        self.assertRaises(ValueError, land.Jungle.set_params,
                          {'alpha': 'string'})

    def testCreateLandscape(self):
        land.Landscape()

    def testCreateMountain(self):
        land.Mountain()

    def testCreateOcean(self):
        land.Ocean()

    def testCreateDesert(self):
        land.Desert()

    def testCurrentFoodJungle(self):
        '''
        Tests current food in the Jungle at the beginning of the simulation.
        '''
        jcell = land.Jungle()
        self.assertEqual(500, jcell.currfood)

    def testFoodGrowthJungle(self):
        jcell = land.Jungle()
        jcell.currfood = 200
        jcell.yearly_growth()
        self.assertEqual(500, jcell.currfood)

    def testFoodDecreaseJungle(self):
        jcell = land.Jungle()
        jcell.food_decrease(10)
        self.assertEqual(490, jcell.currfood)

    def testCurrentFoodSavannah(self):
        '''
        Tests current food in the Savannah at the beginning of the simulation.
        '''
        scell = land.Savannah()
        self.assertEqual(300, scell.currfood)

    def testFoodGrowthSavannah(self):
        scell = land.Savannah()
        scell.currfood = 200
        scell.yearly_growth()
        self.assertEqual(230, scell.currfood)

    def testFoodDecreaseSavannah(self):
        scell = land.Savannah()
        scell.food_decrease(10)
        self.assertEqual(290, scell.currfood)

    def testFoodDecreaseError(self):
        jcell = land.Jungle()
        jcell.currfood = 5
        self.assertRaises(ValueError, jcell.food_decrease, (10))

    def testAddingAnimal(self):
        jcell = land.Jungle()
        mcell = land.Mountain()
        herb = Herbivore()
        self.assertTrue(jcell.add_animal(herb))
        self.assertIn(herb, jcell[Herbivore])
        self.assertFalse(mcell.add_animal(herb))
        self.assertNotIn(herb, mcell[Herbivore])

    def testRemovingAnimal(self):
        jcell = land.Jungle()
        herb = Herbivore()
        jcell.update({Herbivore: [herb]})
        jcell.remove_animal(herb)
        self.assertTrue(not herb in jcell[Herbivore])

    def testSorting(self):
        jcell = land.Jungle()
        h1, h2, h3 = Herbivore(80, 100), Herbivore(4, 40), Herbivore(40, 20)
        jcell.update({Herbivore: [h1, h2, h3]})
        jcell.sort_animals(Herbivore)
        self.assertEqual([h1, h3, h2], jcell[Herbivore])

    def testCountAnimalsHerbivores(self):
        jcell = land.Jungle()
        for _ in range(10):
            jcell.get(Herbivore).append(Herbivore(4, 40))
        jcell.count_animals_in_cell()
        self.assertEqual(10, jcell.number_of_animals[Herbivore])

    def testGetNumberOfAnimals(self):
        jcell = land.Jungle()
        for _ in range(10):
            jcell.get(Herbivore).append(Herbivore(4, 40))
        self.assertEqual(10, jcell.get_number_of_animals()[Herbivore])

    def testFeedingFitnessOrderHerbs(self):
        jcell = land.Jungle()
        jcell.currfood = 15
        h1, h2, h3 = Herbivore(80, 100), Herbivore(4, 40), Herbivore(40, 20)
        jcell.update({Herbivore: [h1, h3, h2]})
        jcell.feeding_herbs()
        self.assertEqual([100, 22, 44], [jcell.get(Herbivore)[0].weight,
                                         jcell.get(Herbivore)[1].weight,
                                         jcell.get(Herbivore)[2].weight])

    def testFeedingFitnessOrderCarns(self):
        jcell = land.Jungle()
        h1, h2, h3 = Herbivore(1000, 1000), Herbivore(4, 40), Herbivore(60, 5)
        c1, c2, c3 = Carnivore(4, 60), Carnivore(4, 20), Carnivore(4, 30)
        Carnivore.change_rand_number(lambda x: 0.0001)
        jcell.update({Herbivore: [h1, h3, h2],
                      Carnivore: [c1, c2, c3]})
        jcell.feeding_carns()
        self.assertEqual([60, 33.75, 31.25], [jcell.get(Carnivore)[0].weight,
                                         jcell.get(Carnivore)[1].weight,
                                         jcell.get(Carnivore)[2].weight])

    def testHerbivoreEats(self):
        jcell = land.Jungle()
        herb = Herbivore()
        jcell.update({Herbivore: [herb]})
        jcell.feeding()
        self.assertEqual(12, herb.weight)

    def testHerbivoreDoesNotEat(self):
        jcell = land.Jungle()
        jcell.currfood = 0
        herb = Herbivore()
        jcell.update({Herbivore: [herb]})
        jcell.feeding_herbs()
        self.assertEqual(8, herb.weight)

    def testFoodReduction(self):
        jcell = land.Jungle()
        herb = Herbivore()
        jcell.update({Herbivore: [herb]})
        jcell.feeding()
        self.assertEqual(490, jcell.currfood)

    def testProcreationCreatesNewAnimals100(self):
        jcell = land.Jungle()
        for _ in range(100):
            jcell.get(Herbivore).append(Herbivore(4, 40))
        jcell.procreation()
        self.assertGreater(len(jcell.get(Herbivore)), 105)

    def testProcreationCreatesNewAnimals2(self):
        jcell = land.Jungle()
        jcell.update({Herbivore: [Herbivore(4, 40), Herbivore(4, 40)]})
        Herbivore.set_params({'gamma': 10})
        jcell.procreation()
        self.assertEqual(4, len(jcell.get(Herbivore)))

    def testGetMigratingAnimals(self):
        jcell = land.Jungle(3, 4)
        herb1, herb2 = Herbivore(4, 40), Herbivore(4, 40)
        herb3, herb4 = Herbivore(4, 40), Herbivore(4, 40)
        jcell.update({Herbivore: [herb1, herb2, herb3, herb4]})
        Herbivore.set_params({'mu': 20})
        rand_nums = [1., 0.15, 1., 0.35, 1., 0.65, 1., 0.85]
        Herbivore.change_rand_number(
                            plmock.FuncReturningValueSequence(rand_nums))
        self.assertEqual([[herb1, (2, 4)], [herb2, (3, 5)],
                          [herb3, (4, 4)], [herb4, (3, 3)]],
                         jcell.get_migrating_animals())

    def testRemoveMigratingAnimals(self):
        jcell = land.Jungle(3, 4)
        herb1, herb2 = Herbivore(), Herbivore()
        carn = Carnivore()
        jcell.update({Herbivore: [herb1, herb2],
                                Carnivore: [carn]})
        jcell.remove_migrating_animals([herb1, herb2, carn])
        self.assertEqual(0, len(jcell.get(Herbivore) + jcell.get(Carnivore)))

    def testAging(self):
        jcell = land.Jungle()
        herb = Herbivore(3)
        jcell.get(Herbivore).append(herb)
        jcell.aging()
        self.assertEqual(4, herb.age)

    def testWeightLoss(self):
        jcell = land.Jungle()
        herb = Herbivore(9, 100)
        jcell.get(Herbivore).append(herb)
        jcell.loss_of_weight()
        self.assertEqual(95, herb.weight)

    def testDeath(self):
        jcell = land.Jungle()
        herb1, herb2 = Herbivore(1, 4), Herbivore(4, 40)
        jcell.get(Herbivore).extend([herb1, herb2])
        jcell.death()
        self.assertEqual([herb2], jcell.get(Herbivore))

    def testPosition(self):
        jcell = land.Jungle(3, 9)
        self.assertEqual((3, 9), jcell.pos)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testCreate']
    unittest.main()
