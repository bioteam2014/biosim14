'''
Created on 9. jan. 2014

@author: kaor
'''
import unittest

from biosim.animals import Herbivore


class Test(unittest.TestCase):

    def testSetParams(self):
        new_w_min = 10
        new_sigma = 0.1
        new_phi_low = 0.3
        new_phi_high = 0.1
        new_w_half_low = 20
        new_w_half_high = 40
        Herbivore.set_params({'w_min': new_w_min,
                             'sigma': new_sigma,
                             'phi_low': new_phi_low,
                             'phi_high': new_phi_high,
                             'w_half_low': new_w_half_low,
                             'w_half_high': new_w_half_high})
        herb = Herbivore()

        self.assertEqual(herb.params['w_min'], new_w_min,
                        'w_min not set correctly')
        self.assertEqual(herb.params['sigma'], new_sigma,
                        'sigma not set correctly')
        self.assertEqual(herb.params['phi_low'], new_phi_low,
                        'phi_low not set correctly')
        self.assertEqual(herb.params['phi_high'], new_phi_high,
                        'phi_high not set correctly')
        self.assertEqual(herb.params['w_half_low'], new_w_half_low,
                        'w_half_low not set correctly')
        self.assertEqual(herb.params['w_half_high'], new_w_half_high,
                        'w_half_high not set correctly')
        self.assertRaises(ValueError, Herbivore.set_params,
                          {'w_min': -0.001})
        self.assertRaises(ValueError, Herbivore.set_params,
                          {'sigma': -0.001})
        self.assertRaises(ValueError, Herbivore.set_params,
                          {'sigma': 1.001})
        self.assertRaises(ValueError, Herbivore.set_params,
                          {'phi_low': 0.05})
        self.assertRaises(ValueError, Herbivore.set_params,
                          {'phi_high': 0.7})
        self.assertRaises(ValueError, Herbivore.set_params,
                          {'phi_low': 0.3, 'phi_high': 0.4})
        self.assertRaises(ValueError, Herbivore.set_params,
                          {'w_half_low': 70})
        self.assertRaises(ValueError, Herbivore.set_params,
                          {'w_half_high': 5})
        self.assertRaises(ValueError, Herbivore.set_params,
                          {'w_half_low': 40, 'w_half_high': 30})
        self.assertRaises(ValueError, Herbivore.set_params,
                          {'omega': 'stupid input'})
        self.assertRaises(KeyError, Herbivore.set_params,
                          {'weightmin': 7})

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testSetParams']
    unittest.main()
