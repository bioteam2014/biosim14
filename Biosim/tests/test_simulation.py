'''
Created on 8. jan. 2014

@author: jonasni
'''
import unittest
from biosim.simulation import Simulation
from biosim.animals import Herbivore, Carnivore


class Test(unittest.TestCase):

    def tearDown(self):
        sim = Simulation()
        self.herb_par = {'w_birth': 8.,
                         'beta': 0.4,
                         'sigma': 0.05,
                         'w_min': 5.,
                         'a_half': 40.,
                         'phi_age': 0.1,
                         'w_half_low': 10.,
                         'w_half_high': 60.,
                         'phi_low': 0.5,
                         'phi_high': 0.2,
                         'mu': 0.15,
                         'gamma': 0.1,
                         'zeta': 2.,
                         'omega': 0.01,
                         'F': 10.}
        sim.set_parameters(self.herb_par)

    def testCreate(self):
        Simulation()

    def testLengthOfCounters(self):
        sim = Simulation()
        herb = [{'loc': (2, 2),
                'pop': [{'species': 'Herbivore',
                         'age': 4,
                         'weight': 40}]}]
        sim.run(10, herb)
        self.assertEqual(11, len(sim.total_nums_of_animals[Herbivore]))
        self.assertEqual(11, len(sim.total_nums_of_animals[Carnivore]))

    def testGetNumbersBySpecies(self):
        sim = Simulation()
        sim.total_nums_of_animals = {Herbivore: [5, 7, 2],
                                     Carnivore: [3, 5, 6]}
        self.assertEqual({'herbivores': 2, 'carnivores': 6},
                         sim.get_number_of_animals_by_species())

    def testCountAnimals(self):
        sim = Simulation()
        sim.grid.cell[0][0].update({Herbivore: [Herbivore()]})
        sim.count_animals()
        self.assertEqual([1], sim.total_nums_of_animals[Herbivore])

    def testStopIfNoAnimals(self):
        sim = Simulation()
        sim.grid.cell[0][0].update({Herbivore: []})
        sim.run(5, [])
        self.assertEqual(1, len(sim.total_nums_of_animals[Herbivore]))

    def testConstantNumberOfAnimals(self):
        sim = Simulation()
        sim.set_parameters({'omega': 0, 'gamma': 0, 'w_min': 0})
        sim.run(100)
        self.assertEqual(sim.total_nums_of_animals[Herbivore][0],
                         sim.total_nums_of_animals[Herbivore][-1])

    def testAllDie(self):
        sim = Simulation()
        sim.set_parameters({'beta': 0})
        sim.run(100)
        self.assertEqual(0, sim.total_nums_of_animals[Herbivore][-1])

    def testCheckPlacementAndAnimalsConditions(self):
        sim = Simulation()
        starting_animals = [{'loc': (2, 2),
                             'pop': []}]
        herb = {'species': 'Herbivore',
                'age': 3,
                'weight': 20}
        for _ in range(35):
            starting_animals[0]['pop'] += [herb]
        sim.run(100, starting_animals)
        self.assertEqual(35, sim.total_nums_of_animals[Herbivore][0])
        self.assertRaises(IndexError,
                          sim.check_placement_conditions, {'loc': (1, 100)})
        self.assertRaises(IndexError,
                          sim.check_placement_conditions, {'loc': (100, 1)})
        self.assertRaises(ValueError,
                          sim.check_animal_conditions,
                          {'pop': [{'weight': -5, 'age': -5}]}, 0)
        self.assertRaises(ValueError,
                          sim.check_animal_conditions,
                          {'pop': [{'weight': -5, 'age': -5}]}, 0)

    def testCreatePop(self):
        sim = Simulation()
        sim.run(1)
        self.assertGreater(sim.total_nums_of_animals[Herbivore][0], 0)
        self.assertEqual(0, sim.total_nums_of_animals[Carnivore][0])
        sim.run(1)
        self.assertGreater(sim.total_nums_of_animals[Carnivore][2], 0)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testCreate']
    unittest.main()
